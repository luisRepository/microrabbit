﻿using MicroRabbit.Banking.Application.Models;
using MicroRabbit.Banking.Domain.Models;
using System.Collections.Generic;

namespace MicroRabbit.Banking.Application.Interfaces
{
    public interface IAccountService
    {
        #region Interfaces

        IEnumerable<Account> GetAccount();

        void Transfer(AccountTransferDto accountTransferDto);

        #endregion
    }
}