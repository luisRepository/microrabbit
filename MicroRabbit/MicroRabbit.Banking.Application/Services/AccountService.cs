﻿using MicroRabbit.Banking.Application.Interfaces;
using MicroRabbit.Banking.Application.Models;
using MicroRabbit.Banking.Domain.Commands;
using MicroRabbit.Banking.Domain.Interfaces;
using MicroRabbit.Banking.Domain.Models;
using MicroRabbit.Domain.Core.Bus;
using System.Collections.Generic;

namespace MicroRabbit.Banking.Application.Services
{
    public class AccountService : IAccountService
    {
        #region Properties

        private readonly IAccountRepository accountRepository;
        private readonly IEventBus bus;

        #endregion

        #region Constructor

        public AccountService(IAccountRepository accountRepository, IEventBus bus)
        {
            this.accountRepository = accountRepository;
            this.bus = bus;
        }

        #endregion

        #region Operations

        public IEnumerable<Account> GetAccount()
        {
            return accountRepository.GetAccounts();
        }

        public void Transfer(AccountTransferDto accountTransferDto)
        {
            var createTransferCommand = new CreateTransferCommand(
                accountTransferDto.FromAccount,
                accountTransferDto.ToAccount,
                accountTransferDto.TransferAmount
                );

            bus.SendCommand(createTransferCommand);
        }

        #endregion
    }
}