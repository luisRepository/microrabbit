﻿using MicroRabbit.Domain.Core.Events;
using System;

namespace MicroRabbit.Domain.Core.Commands
{
    public abstract class Command: Message
    {
        #region Properties

        public DateTime Timestamp { get; protected set; }

        #endregion

        #region Constructor

        protected Command()
        {
            Timestamp = DateTime.Now;
        }

        #endregion
    }
}