﻿using MicroRabbit.Domain.Core.Events;
using System.Threading.Tasks;

namespace MicroRabbit.Domain.Core.Bus
{
    public interface IEventHandler<in TEvent> : IEventHandler where TEvent : Event
    {
        #region Interfaces

        Task Handler(TEvent @event);

        #endregion
    }

    public interface IEventHandler
    {

    }
}