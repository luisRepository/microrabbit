﻿using System;

namespace MicroRabbit.Domain.Core.Events
{
    public abstract class Event
    {
        #region Properties

        public DateTime Timestamp { get; protected set; }

        #endregion

        #region Constructor

        protected Event()
        {
            Timestamp = DateTime.Now;
        }

        #endregion
    }
}