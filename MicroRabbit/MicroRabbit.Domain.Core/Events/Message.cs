﻿using MediatR;

namespace MicroRabbit.Domain.Core.Events
{
    public abstract class Message : IRequest<bool>
    {
        #region Properties

        public string MessageType { get; protected set; }

        #endregion

        #region Constructor

        protected Message()
        {
            MessageType = GetType().Name;
        }

        #endregion
    }
}