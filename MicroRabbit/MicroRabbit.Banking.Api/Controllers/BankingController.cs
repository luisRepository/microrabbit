﻿using MicroRabbit.Banking.Application.Interfaces;
using MicroRabbit.Banking.Application.Models;
using MicroRabbit.Banking.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MicroRabbit.Banking.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankingController : ControllerBase
    {

        #region Properties

        private readonly IAccountService accountService;

        #endregion

        #region Constructor

        public BankingController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        #endregion

        #region Operations

        // GET api/banking
        [HttpGet]
        public ActionResult<IEnumerable<Account>> Get()
        {
            return Ok(accountService.GetAccount());
        }

        [HttpPost]
        public ActionResult Post([FromBody]AccountTransferDto accountTransferDto)
        {
            accountService.Transfer(accountTransferDto);
            return Ok(accountTransferDto);
        }

        #endregion
    }
}