﻿using MicroRabbit.Banking.Domain.Models;
using System.Collections.Generic;

namespace MicroRabbit.Banking.Domain.Interfaces
{
    public interface IAccountRepository
    {
        #region Interfaces

        IEnumerable<Account> GetAccounts();

        #endregion
    }
}