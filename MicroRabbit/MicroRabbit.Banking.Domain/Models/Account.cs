﻿namespace MicroRabbit.Banking.Domain.Models
{
    public class Account
    {
        #region Properties

        public int Id { get; set; }

        public string AccountType { get; set; }

        public decimal AccountBalance { get; set; }

        #endregion
    }
}