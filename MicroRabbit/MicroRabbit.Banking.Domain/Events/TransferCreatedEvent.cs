﻿using MicroRabbit.Domain.Core.Events;

namespace MicroRabbit.Banking.Domain.Events
{
    public class TransferCreatedEvent: Event
    {

        #region Properties

        public int From { get; private set; }

        public int To { get; private set; }

        public decimal Amount { get; private set; }

        #endregion

        #region Constructor

        public TransferCreatedEvent(int from, int to, decimal amount)
        {
            From = from;
            To = to;
            Amount = amount;
        }

        #endregion
    }
}