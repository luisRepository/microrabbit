﻿using MicroRabbit.Banking.Data.Context;
using MicroRabbit.Banking.Domain.Interfaces;
using MicroRabbit.Banking.Domain.Models;
using System.Collections.Generic;

namespace MicroRabbit.Banking.Data.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        #region Properties

        private readonly BankingDbContext context;

        #endregion

        #region Constructor

        public AccountRepository(BankingDbContext context)
        {
            this.context = context;
        }

        #endregion

        #region Operations

        public IEnumerable<Account> GetAccounts()
        {
            return context.Accounts;
        }

        #endregion
    }
}