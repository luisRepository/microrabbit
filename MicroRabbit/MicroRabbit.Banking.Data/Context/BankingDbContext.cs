﻿using MicroRabbit.Banking.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroRabbit.Banking.Data.Context
{
    public class BankingDbContext : DbContext
    {
        #region Constructor

        public BankingDbContext(DbContextOptions options) : base(options)
        {
        }

        #endregion

        #region Properties

        public DbSet<Account> Accounts { get; set; }

        #endregion
    }
}